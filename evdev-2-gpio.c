#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/input.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "libevdev/libevdev.h"

#define buttonmap_size 16

typedef struct buttonmap {
	int btn_code;
	int btn_value;
	char gpio_name;
	char gpio_value;
} buttonmap;

buttonmap buttonmaps[buttonmap_size];

char gpios[buttonmap_size];


void write_gpio(char gpio, char value) {
	int fd;

	if (value != 0 && value != 1) {
		fprintf(stderr, "value not 1 or 0");
		exit(1);
	}

	char gpiopath[32];
	sprintf(gpiopath, "/sys/class/gpio/gpio%d/value", gpio);
	fd = open(gpiopath, O_WRONLY);
	if (fd == -1) {
		char errormessage[48];
		sprintf(errormessage, "Unable to open %s\n", gpiopath);
		perror(errormessage);
		exit(1);
	}
	char newvalue = value + 48;
	printf("merkki: %c\n", newvalue);
	if (write(fd, &newvalue, 1) != 1) {
		char errormessage[50];
		sprintf(errormessage, "Error writing to %s\n", gpiopath);
		perror(errormessage);
		exit(1);
	}
	close(fd);
}

void export_gpio(char gpioname) {
	int fd;
	char gpio_string[4];
	printf("Exporting gpio%d\n", gpioname);
	sprintf(gpio_string, "%d", gpioname);

	fd = open("/sys/class/gpio/export", O_WRONLY);
	if (fd == -1) {
		perror("Unable to open /sys/class/gpio/export");
		exit(1);
	}
	if (write(fd, gpio_string, strlen(gpio_string)) != strlen(gpio_string)) {
		perror("Error writing to /sys/class/gpio/export");
		exit(1);
	}
	close(fd);
}

void set_gpio_direction(char gpioname) {
	int fd;
	char gpio_direction_path[34];
	printf("Setting gpio%d direction\n", gpioname);
	sprintf(gpio_direction_path, "/sys/class/gpio/gpio%d/direction", gpioname);

	fd = open(gpio_direction_path, O_WRONLY);
	if (fd == -1) {
		char errormessage[50];
		sprintf(errormessage, "Unable to open %s\n", gpio_direction_path);
		perror(errormessage);
		exit(1);
	}
	if (write(fd, "out", 3) != 3) {
		char errormessage[52];
		sprintf(errormessage, "Error writing to %s\n", gpio_direction_path);
		perror(errormessage);
		exit(1);
	}
	close(fd);
}

void unexport_gpio(char gpioname) {
	int fd;
	char gpio_string[4];
	printf("Unexporting gpio%d\n", gpioname);
	sprintf(gpio_string, "%d", gpioname);

	fd = open("/sys/class/gpio/unexport", O_WRONLY);
	if (fd == -1) {
		perror("Unable to open /sys/class/gpio/unexport");
		exit(1);
	}
	if (write(fd, gpio_string, strlen(gpio_string)) != strlen(gpio_string)) {
		perror("Error writing to /sys/class/gpio/unexport");
		exit(1);
	}
	close(fd);
}

void initialize_gpio() {
	for(int i=0; i<buttonmap_size; i++) {
		if(gpios[i] == 0)
			return;
		export_gpio(gpios[i]);
		set_gpio_direction(gpios[i]);
	}
}

void make_gpio_list() {
	for(int i=0; i<buttonmap_size; i++) {
		gpios[i] = 0;
	}
	for(int i=0; i<buttonmap_size; i++) {
		char current_gpio = buttonmaps[i].gpio_name;
		printf("current_gpio: %d\n", current_gpio);
		for(int j=0; j<buttonmap_size; j++) {
			if(gpios[j] == current_gpio) {
				break;
			}
			if(gpios[j] == 0) {
				gpios[j] = current_gpio;
				break;
			}
		}
			
	}
	for(int i=0; i<buttonmap_size; i++) {
		printf("gpio: %d\n", gpios[i]);
	}

}

void cleanup() {
	for(int i=0; i<buttonmap_size; i++) {
		if(gpios[i] == 0)
			return;
		unexport_gpio(gpios[i]);
	}
}

void exit_nicely() {
	printf("\nStopping...\n");
	exit(0);
}

void main() {
	struct libevdev *dev = NULL;
	int fd;
	int rc = 1;

	atexit(cleanup);
	signal(SIGINT, exit_nicely);


	buttonmaps[0] = (buttonmap){
		.btn_code = BTN_SOUTH,
		.btn_value = 1,
		.gpio_name = 198,
		.gpio_value = 1
	};
	buttonmaps[1] = (buttonmap){
		.btn_code = BTN_SOUTH,
		.btn_value = 0,
		.gpio_name = 198,
		.gpio_value = 0
	};
	buttonmaps[2] = (buttonmap){
		.btn_code = BTN_EAST,
		.btn_value = 1,
		.gpio_name = 199,
		.gpio_value = 1
	};
	buttonmaps[3] = (buttonmap){
		.btn_code = BTN_EAST,
		.btn_value = 0,
		.gpio_name = 199,
		.gpio_value = 0
	};
	buttonmaps[4] = (buttonmap){
		.btn_code = ABS_HAT0Y,
		.btn_value = -1,
		.gpio_name = 20,
		.gpio_value = 1
	};
	buttonmaps[5] = (buttonmap){
		.btn_code = ABS_HAT0Y,
		.btn_value = 0,
		.gpio_name = 20,
		.gpio_value = 0
	};
	buttonmaps[6] = (buttonmap){
		.btn_code = ABS_HAT0Y,
		.btn_value = 1,
		.gpio_name = 10,
		.gpio_value = 1
	};
	buttonmaps[7] = (buttonmap){
		.btn_code = ABS_HAT0Y,
		.btn_value = 0,
		.gpio_name = 10,
		.gpio_value = 0
	};
	buttonmaps[8] = (buttonmap){
		.btn_code = ABS_HAT0X,
		.btn_value = -1,
		.gpio_name = 9,
		.gpio_value = 1
	};
	buttonmaps[9] = (buttonmap){
		.btn_code = ABS_HAT0X,
		.btn_value = 0,
		.gpio_name = 9,
		.gpio_value = 0
	};
	buttonmaps[10] = (buttonmap){
		.btn_code = ABS_HAT0X,
		.btn_value = 1,
		.gpio_name = 8,
		.gpio_value = 1
	};
	buttonmaps[11] = (buttonmap){
		.btn_code = ABS_HAT0X,
		.btn_value = 0,
		.gpio_name = 8,
		.gpio_value = 0
	};
	buttonmaps[12] = (buttonmap){
		.btn_code = BTN_TL,
		.btn_value = 1,
		.gpio_name = 200,
		.gpio_value = 1
	};
	buttonmaps[13] = (buttonmap){
		.btn_code = BTN_TL,
		.btn_value = 0,
		.gpio_name = 200,
		.gpio_value = 0
	};
	buttonmaps[14] = (buttonmap){
		.btn_code = BTN_TR,
		.btn_value = 1,
		.gpio_name = 201,
		.gpio_value = 1
	};
	buttonmaps[15] = (buttonmap){
		.btn_code = BTN_TR,
		.btn_value = 0,
		.gpio_name = 201,
		.gpio_value = 0
	};
	make_gpio_list();

	initialize_gpio();
	
	//exit(0);

	fd = open("/dev/input/event1", O_RDONLY|O_NONBLOCK);
	rc = libevdev_new_from_fd(fd, &dev);
	if (rc < 0) {
	        fprintf(stderr, "Failed to init libevdev (%s)\n", strerror(-rc));
	        exit(1);
	}
	printf("Input device name: \"%s\"\n", libevdev_get_name(dev));
	printf("Input device ID: bus %#x vendor %#x product %#x\n",
	       libevdev_get_id_bustype(dev),
	       libevdev_get_id_vendor(dev),
	       libevdev_get_id_product(dev));
	if (!libevdev_has_event_type(dev, EV_ABS) ||
	    !libevdev_has_event_code(dev, EV_ABS, ABS_HAT0X)) {
	        printf("This device does not look like a gamepad\n");
	        exit(1);
	}
	
	do {
	        struct input_event ev;
	        rc = libevdev_next_event(dev, LIBEVDEV_READ_FLAG_NORMAL, &ev);
	        if (rc == 0) {
			printf("Event: %d %d %d\n",ev.type, ev.code, ev.value);
			if (ev.code == 0) continue;
			for(int i=0; i<buttonmap_size; i++) {
				if (ev.code == buttonmaps[i].btn_code && ev.value == buttonmaps[i].btn_value) {
					write_gpio(buttonmaps[i].gpio_name, buttonmaps[i].gpio_value);
				}
					
			}
		}
	} while (rc == 1 || rc == 0 || rc == -EAGAIN);
}
